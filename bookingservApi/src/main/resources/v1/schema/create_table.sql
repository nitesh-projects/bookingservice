CREATE TABLE IF NOT EXISTS BOOKING(
    id numeric(10), not null
    first_name varchar(255) not null,
    last_name varchar(255) not null
    primary key (id)
);