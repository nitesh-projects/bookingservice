package com.paypal.bfs.test.bookingserv.dao;

import com.paypal.bfs.test.bookingserv.entity.BookingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookingSerRepository  extends JpaRepository<BookingEntity, BookingEntity> {
}
