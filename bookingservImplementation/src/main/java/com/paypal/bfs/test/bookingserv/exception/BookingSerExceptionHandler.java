package com.paypal.bfs.test.bookingserv.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class BookingSerExceptionHandler {
    @ExceptionHandler(value = {RuntimeException.class})
    public ResponseEntity<ErrorMessage> runtimeExceptionHandler(RuntimeException ex, WebRequest request) {
        return new ResponseEntity<ErrorMessage>(ErrorMessage.builder().casue(ex.getCause()).message(ex.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
