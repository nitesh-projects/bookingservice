package com.paypal.bfs.test.bookingserv.exception;

import lombok.Builder;
import lombok.Getter;
import net.bytebuddy.implementation.bytecode.Throw;

@Builder
@Getter
public class ErrorMessage {
    private String message;

    private Throwable casue;
}
