package com.paypal.bfs.test.bookingserv.impl;

import com.paypal.bfs.test.bookingserv.api.BookingResource;
import com.paypal.bfs.test.bookingserv.api.model.Booking;
import com.paypal.bfs.test.bookingserv.service.BookingSerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.validation.constraints.NotNull;

public class BookingResourceImpl implements BookingResource {

    private final BookingSerService bookingSerService;

    @Autowired
    public BookingResourceImpl(BookingSerService bookingSerService){
        this.bookingSerService = bookingSerService;
    }

    @Override
    public ResponseEntity<Booking> create(@NotNull  Booking booking) {
        return new ResponseEntity<>(bookingSerService.create(booking), HttpStatus.CREATED);
    }
}
