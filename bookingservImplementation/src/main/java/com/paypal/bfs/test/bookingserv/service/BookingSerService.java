package com.paypal.bfs.test.bookingserv.service;

import com.paypal.bfs.test.bookingserv.api.model.Booking;
import com.paypal.bfs.test.bookingserv.dao.BookingSerRepository;
import com.paypal.bfs.test.bookingserv.entity.BookingEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookingSerService {

    private final BookingSerRepository bookingSerRepository;

    @Autowired
    public BookingSerService(BookingSerRepository bookingSerRepository){

        this.bookingSerRepository = bookingSerRepository;
    }

    public Booking create(Booking booking){
        validateBooking(booking);
        BookingEntity entity = BookingEntity.builder().id(booking.getId()).firstName(booking.getFirstName()).lastName(booking.getFirstName()).build();
        bookingSerRepository.save(entity);
        return booking;
    }

    private void validateBooking(Booking booking) {
        if(booking.getId()==null){
            throw new RuntimeException("Id cannot be null");
        }

        if(booking.getFirstName()==null){
            throw new RuntimeException("First Name cannot be null");
        }

        if(booking.getLastName()==null){
            throw new RuntimeException("Last Name cannot be null");
        }
    }

}
